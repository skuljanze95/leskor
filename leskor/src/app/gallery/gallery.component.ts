import { Component, OnInit } from "@angular/core";
import { IMasonryGalleryImage } from "ngx-masonry-gallery";

@Component({
  selector: "app-gallery",
  templateUrl: "./gallery.component.html",
  styleUrls: ["./gallery.component.scss"]
})
export class GalleryComponent implements OnInit {
  public mobile;
  constructor() {}

  ngOnInit() {
    if (window.screen.width <= 360) {
      // 768px portrait
      this.mobile = true;
    }
  }

  private urls: string[] = [
    "https://i.imgur.com/AQ4WJS3.jpg",
    "https://i.imgur.com/c8q409a.jpg",
    "https://i.imgur.com/IFPFlt9.jpg",

    "https://i.imgur.com/bjCAd99.jpg",
    "https://i.imgur.com/uZZqd2d.jpg",
    "https://i.imgur.com/4r9iUSa.jpg",
    "https://i.imgur.com/pwjJaOs.jpg",
    "https://i.imgur.com/NInzxRz.jpg",
    "https://i.imgur.com/o9630SM.jpg",
    "https://i.imgur.com/OCwuyRW.jpg",
    "https://i.imgur.com/e5iK0Rq.jpg",
    "https://i.imgur.com/deq4kU6.jpg",
    "https://i.imgur.com/ANaWjRf.jpg",
    "https://i.imgur.com/Yapw4Cy.jpg",
    "https://i.imgur.com/92s1Uhq.jpg",
    "https://i.imgur.com/kSQzHvC.jpg",
    "https://i.imgur.com/5k8iHOj.jpg",
    "https://i.imgur.com/bfQJesR.jpg",
    "https://i.imgur.com/twfQCo2.jpg",
    "https://i.imgur.com/abzFqEg.jpg",
    "https://i.imgur.com/MaaczUM.jpg",
    "https://i.imgur.com/mZfM9ss.jpg",
    "https://i.imgur.com/0qhy5wE.jpg",
    "https://i.imgur.com/s1ST2Tp.jpg",
    "https://i.imgur.com/WuJcq0W.jpg",
    "https://i.imgur.com/UjBDbnD.jpg",
    "https://i.imgur.com/2B2PYLz.jpg",
    "https://i.imgur.com/bqzkxlA.jpg",
    "https://i.imgur.com/G3pQROr.jpg",
    "https://i.imgur.com/kOlhXil.jpg",
    "https://i.imgur.com/1CqYc62.jpg",
    "https://i.imgur.com/4KZCv9W.jpg",
    "https://i.imgur.com/dJ92Mp8.jpg",
    "https://i.imgur.com/v4pgOzs.jpg",
    "https://i.imgur.com/t5uwock.jpg",
    "https://i.imgur.com/koTHqV1.jpg",
    "https://i.imgur.com/adwW8cX.jpg",
    "https://i.imgur.com/6PV14ei.jpg",
    "https://i.imgur.com/6UzclyW.jpg",
    "https://i.imgur.com/0Io7c7h.jpg",
    "https://i.imgur.com/T9Cpik6.jpg",
    "https://i.imgur.com/nDxQzdi.jpg",
    "https://i.imgur.com/uARa3Qp.jpg",
    "https://i.imgur.com/ceE6MXn.jpg",
    "https://i.imgur.com/559T1tb.jpg",
    "https://i.imgur.com/fJDAxla.jpg",
    "https://i.imgur.com/JJsx0uk.jpg",
    "https://i.imgur.com/g6QgvBB.jpg",
    "https://i.imgur.com/LPwsreL.jpg",
    "https://i.imgur.com/b3MJlWX.jpg",
    "https://i.imgur.com/hG6OCgV.jpg",
    "https://i.imgur.com/WJASv9R.jpg",
    "https://i.imgur.com/ylceq8n.jpg",
    "https://i.imgur.com/eZzIeCa.jpg",
    "https://i.imgur.com/HbdLjpF.jpg",
    "https://i.imgur.com/SFOYrze.jpg",
    "https://i.imgur.com/WGWokf8.jpg",
    "https://i.imgur.com/csOM1Ub.jpg",
    "https://i.imgur.com/6JJPB4w.jpg",
    "https://i.imgur.com/LbnOor2.jpg",

    "https://i.imgur.com/1BRHZEd.jpg",

    "https://i.imgur.com/iUWAIRo.jpg",
    "https://i.imgur.com/yZKQvM3.jpg",
    "https://i.imgur.com/hzvp3OZ.jpg",
    "https://i.imgur.com/xAYy7CL.jpg",
    "https://i.imgur.com/GkCTiWB.jpg",

    "https://i.imgur.com/scitgQN.jpg",
    "https://i.imgur.com/1DMXxAG.jpg",
    "https://i.imgur.com/ycey3AA.jpg",
    "https://i.imgur.com/47P7Nk1.jpg",
    "https://i.imgur.com/E6CFEoX.jpg",
    "https://i.imgur.com/Tsm8UJN.jpg",
    "https://i.imgur.com/hrSoC4d.jpg",
    "https://i.imgur.com/JAr0OEg.jpg",
    "https://i.imgur.com/wBGYFQL.jpg",
    "https://i.imgur.com/u9QPyoA.jpg",
    "https://i.imgur.com/02I3evz.jpg",
    "https://i.imgur.com/eYCPOFR.jpg",
    "https://i.imgur.com/hVlzJmD.jpg",
    "https://i.imgur.com/VGGBfWu.jpg"
  ];

  public get images(): IMasonryGalleryImage[] {
    return this.urls.map(
      m =>
        <IMasonryGalleryImage>{
          imageUrl: m
        }
    );
  }
}
