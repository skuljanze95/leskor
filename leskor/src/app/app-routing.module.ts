import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { HomeComponent } from "./home/home.component";
import { AboutComponent } from "./about/about.component";
import { GalleryComponent } from "./gallery/gallery.component";
import { ContactComponent } from "./contact/contact.component";
import { ProjetcsComponent } from "./projetcs/projetcs.component";
import { NotFoundComponent } from "./not-found/not-found.component";
import { PrivacyComponent } from "./privacy/privacy.component";
import { JobsComponent } from "./jobs/jobs.component";

const routes: Routes = [
  { path: "", component: HomeComponent },
  { path: "o-nas", component: AboutComponent },
  { path: "galerija", component: GalleryComponent },
  { path: "kontakt", component: ContactComponent },
  { path: "projekti/:id", component: ProjetcsComponent },
  { path: "404", component: NotFoundComponent },
  { path: "zasebnost", component: PrivacyComponent },
  { path: "zaposlitev", component: JobsComponent },
  { path: "**", redirectTo: "/404" }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
