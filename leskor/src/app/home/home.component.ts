import { Component, OnInit } from "@angular/core";
import { SwiperOptions } from "swiper";
import { Observable, concat } from "rxjs";

import "firebase/firestore";
import { AngularFirestore } from "@angular/fire/firestore";
import { MatDialog } from "@angular/material/dialog";
import { ProductDialogComponent } from "../product-dialog/product-dialog.component";

import { Validators } from "@angular/forms";

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.scss"],
})
export class HomeComponent implements OnInit {
  items: Observable<any[]>;

  emailInfo: string = "";

  config: SwiperOptions = {
    pagination: { el: ".swiper-pagination", clickable: true },
    slidesPerView: 3,
    spaceBetween: 30,
    slidesPerGroup: 1,
    autoplay: true,
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev",
    },
  };

  constructor(public firestore: AngularFirestore, public dialog: MatDialog) {
    this.items = firestore.collection("Projects").valueChanges();
  }

  ngOnInit() {}

  openDialog(item): void {
    let dialogRef = this.dialog.open(ProductDialogComponent, {
      maxWidth: "100vw",
      width: "50%",
      data: item,
    });

    dialogRef.afterClosed().subscribe((result) => {});
  }

  displayMessage() {
    this.emailInfo = "Vaš e-mail je shranjen";
    setTimeout(() => {
      this.emailInfo = "";
    }, 2000);
  }

  onSubmit(test: string) {
    event.preventDefault();
    this.displayMessage();
    this.firestore
      .collection("email")
      .doc(test)
      .set({
        email: test,
      })
      .then(function () {
        console.log("Document successfully written!");

        console.log("!");
      })
      .catch(function (error) {});
  }
}
