import { Component, OnInit } from "@angular/core";
import { Observable } from "rxjs";
import { AngularFirestore } from "@angular/fire/firestore";
import { Router, ActivatedRoute } from "@angular/router";
import { MatDialog } from "@angular/material/dialog";
import { ProductDialogComponent } from "../product-dialog/product-dialog.component";
import "firebase/firestore";

@Component({
  selector: "app-projetcs",
  templateUrl: "./projetcs.component.html",
  styleUrls: ["./projetcs.component.scss"],
})
export class ProjetcsComponent implements OnInit {
  items: Observable<any[]>;
  itemValue = "";
  filterdata: any;
  id: any;

  constructor(
    public firestore: AngularFirestore,
    public router: Router,
    private route: ActivatedRoute,
    public dialog: MatDialog
  ) {
    this.items = firestore.collection("Projects").valueChanges();
  }

  ngOnInit() {
    this.id = this.route.params.subscribe((params) => {
      this.filterdata = params["id"];
    });
  }

  onSubmit(name: string, picture: string, category: string) {
    event.preventDefault();
    this.firestore.collection("Projects").add({
      name: name,
      picture: picture,
      category: category,
    });
    //console.log(this.itemValue);
    this.itemValue = "";
  }
  openDialog(item): void {
    let dialogRef = this.dialog.open(ProductDialogComponent, {
      maxWidth: "100vw",
      width: "50%",
      data: item,
    });

    dialogRef.afterClosed().subscribe((result) => {});
  }
}
