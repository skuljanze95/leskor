import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { HomeComponent } from "./home/home.component";
import { AboutComponent } from "./about/about.component";
import { GalleryComponent } from "./gallery/gallery.component";
import { ContactComponent } from "./contact/contact.component";
import { ProjetcsComponent } from "./projetcs/projetcs.component";
import { NotFoundComponent } from "./not-found/not-found.component";
import { PrivacyComponent } from "./privacy/privacy.component";
import { JobsComponent } from "./jobs/jobs.component";

import { MasonryGalleryModule } from "ngx-masonry-gallery";
import { NgxUsefulSwiperModule } from "ngx-useful-swiper";
import { FormsModule } from "@angular/forms";

import { AngularFireModule } from "@angular/fire";
import { environment } from "../environments/environment";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { MatDialogModule } from "@angular/material/dialog";
import { ProductDialogComponent } from "./product-dialog/product-dialog.component";

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AboutComponent,

    GalleryComponent,
    ContactComponent,
    ProjetcsComponent,
    NotFoundComponent,
    PrivacyComponent,
    JobsComponent,
    ProductDialogComponent,
  ],
  imports: [
    MatDialogModule,
    BrowserModule,
    AppRoutingModule,
    MasonryGalleryModule,
    NgxUsefulSwiperModule,
    AngularFireModule.initializeApp(environment.firebase),
    FormsModule,
    BrowserAnimationsModule,
  ],
  entryComponents: [ProductDialogComponent],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
